# HELLO ! :raising_hand:

**THIS IS**   
**SHADY's**  
**BIO**  

*PS : It can be difficult to introduce yourself because you know yourself so well that you don't know where to begin.  
Let me try to see what kind of image you have of me based on my self-description.*

<img src="assets/Screenshot_2022-04-16_231810.png" width=30% height=30%>  

 

|||||||||||||||||||||||||||||||||| **This Is Me** |||||||||||||||||||||||||||||||||||

*“Hey, I’m Shady Saleh . I grew up in Saudi Arabia but I am Egyptian , and lived there most of my life before moving to Egypt to start my Architecture journey at the German University in Cairo. I speak Arabic, English and lately some German. I love horse riding , and tend to socialize with people on daily basics , but this doesn't prevent me from wrapping myself in a blanket and binge-watch series without feeling guilty!”*  
### So...  

Lets start with my **EDUCATION** :  

**2015 /.** Finished my IGCSE's  :heavy_check_mark:  
**2020 /.** Finished my Bachelor Degree in Architecture and Urban Design  :heavy_check_mark:  
**2022 /.** Started my Masters Degree in Construction and Robotics  :wavy_dash:  

**WORK EXPERIENCE** :

* **3D CAD Designer at [360 Imaging](https://360imaging.com/)**  
*There i shifted a bit away from Architecture due to my huge urge to be involved in tech development . My job was to create FDA approved surgical dental guides for implant surgeries. Nothing related to Architecture but i had the chance to be creative.*  
* **Industrial Site Engineer at [Chema Foam](https://www.chema-foam.com/)**  
*For a year I had the chance to get hands-on experince in steel factories constrution sites. What I saw and experinced there was the main motivation to undergo this masters program .*
* **Interior Deginer at [Al-Futtaim IKEA](https://www.alfuttaim.com/)**
*For another year I wanted to experince small scale creations with an extra touch of elegance and improving living situations. So before coming to Aachen I decided to just give interior desiging a chance.*  

**DIGITAL SKILLS** :  

| Program      | Competence |
| ----------- | ----------- |
| AutoCad      | :black_small_square: :black_small_square: :black_small_square: :black_small_square:       |
| BIM Revit   | :black_small_square: :black_small_square: :black_small_square: :black_small_square:        |
| Rhino   | :black_small_square: :black_small_square: :black_small_square: :black_small_square:        |
| Grasshopper   | :black_small_square: :black_small_square:        |
| Photoshop   | :black_small_square: :black_small_square: :black_small_square: :black_small_square:        |
| Illustrator   | :black_small_square: :black_small_square:        |  

______

#### Now I will present to you my self **SWOT** analysis *( a study undertaken to identify internal strengths and weaknesses, as well as its external opportunities and threats.)* :


<img src="assets/SWOT1__1_.png">


#### **Last** thing about the career me  **My Portfolio** :
 
<img src="assets/ezgif.com-gif-maker.gif" width=70% height=70%>

#### Yes it is fast .. but if you are intrestend to check it out do to my [ISSU](https://issuu.com/shmkamal/docs/shadykamalportfolio_compressed__2_) page .
___


## Moving on to the fun me.  

### As I said I love horse riding so let me show you some of what I experience back in Egypt :

<img src="assets/ezgif.com-gif-maker__4_.gif" width=70% height=70%>

### As for my constant urge to travel and get in touch with diffrent cultures here are some of them :

<img src="assets/ezgif.com-gif-maker__1_.gif" width=70% height=70%>  

### TO SUM UP ME :


```plantuml
@startmindmap
* ME
	* FUN ME
		* SOCIALIZING
		* HORSE RIDING
		* TRAVELING
	* CAREER ME
@endmindmap
```

## I think that is all I can say about myself.  
# BYE. :v:




