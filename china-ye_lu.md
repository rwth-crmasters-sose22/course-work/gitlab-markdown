#### **About myself**
I am a 28 year old architect. But I also have some experience in interior design and urban  
design at the same time. I have been working in the **architecture and construction industry**   
since I graduated from university until the beginning of this year.

* Nationality: CHINA![cn](https://raw.githubusercontent.com/gosquared/flags/master/flags/flags/shiny/24/China.png)
* Main work city: Shanghai  :office: Chengdu :panda_face:  

<details>
<summary><b>EMPLOYMENT</b> (click to show)</summary>  

1. [**East China Architectural Design and Research Institute(ECADI)**](http://www.ecadi.com/) 20.05.2017 - 30.06.2018  
2. [**Fusion Architects**](https://cargocollective.com/fusion-arch)                                             01.07.2018 - 30.08.2020   
3. [**MONOARCHI Architects**](https://www.monoarchi.com/)                                          01.01.2021 - 01.03.2022  
</details>

<details>
<summary><b>COMPLETED PROJECTS</b> (click to show)</summary>

1. [Hamilton Primary School & Kindergarten / Fusion Architects](https://www.archdaily.com/960473/hamilton-primary-school-and-kindergarten-fusion-architects?ad_source=search&ad_medium=search_result_projects)
2. Wonder Mountain Cloud Land Kindergarten, Chengdu / Fusion Architects  
_It hasn't been published yet_
3. [Design Shanghai 2021,AD China Magazine Exhibition / MONOARCHI](https://www.sohu.com/a/470371583_121124644)
</details>

**LANGUAGE SKILLS**  
- [x] CHINESE
- [x] ENGLISH
- [ ] GERMAN

![Former workplace](https://media-exp1.licdn.com/dms/image/C5622AQHq14MX9A9LoQ/feedshare-shrink_800/0/1650400405335?e=2147483647&v=beta&t=fC-unv95iQd8qDyU6P9PFgjgVD_2B4QoC1-WLBI4nNc)
>In the past five years, I have participated in many large-scale construction projects, such as railway stations, skyscrapers and so on. To be honest, I don't feel like a designer of architecture, but one of the engineers in the huge construction industry. Nowadays, China's construction projects still stay in the labor-intensive design and construction mode. Although we will establish the BIM system, often after a project is completed, the BIM system has just been completed, which is just similar to the inspection of completed projects, and a large number of drawings are still drawn by manpower. Therefore, even if the architectural design is very exquisite, it will often be rough in the construction stage. I often act as a project manager and check the progress of the project on the construction site. Almost every time, I can return to the company with a lot of construction problems.
The labor cost is really cheap in China, but the accuracy of construction can't be appreciated, and many workers can't understand our drawings.
With the development of the global epidemic, China's economy has inevitably had a little impact. Our company can receive fewer and fewer city level projects, most of which are interior design and old building reconstruction projects. This gave me the idea of ​​further study.
Smart industry is an inevitable trend. So here I am, RWTH. Although this is a great challenge, I am willing to try it.

![hemert](https://media-exp1.licdn.com/dms/image/C5622AQFIF0_eRzT7Bw/feedshare-shrink_800/0/1650401600158?e=2147483647&v=beta&t=sdRkcn2D19YC55niL2hnQcNeUjThJoKdtAilqA-ZV3I)

I am trying to improve my English and study computer science.Welcome to share your experience with me!:mailbox_with_no_mail: :mailbox_with_no_mail:  
<ye.lu@rwth-aachen.de>
