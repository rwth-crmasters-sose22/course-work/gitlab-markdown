# Hello All :smiley:

## My background 
My name is **Sushmitha Pais** and I am from India 🇮🇳  
* I have completed my _Bachelor's in Computer Applications_
* I worked at _Mckinsey & Company_ for about a year
* I also have some experience in tutoring school children

## My Professional skills include 👔
* **Technical skills**: C, Java, SQL, Web Development, Python, Mobile
  Application Development, Database Management Systems, Software
  Engineering, Software Testing
* **Microsoft Office suite**: Excel, PowerPoint Presentation, Word, Outlook
* **German language** Level B2.1 at Goethe-Institut Max Mueller Bhavan, Bengaluru
* Working knowledge on IBM notes, Thinkcell charts at McKinsey and Co

## My Projects & Workshops :computer:

```mermaid  


graph TD
    A[Projects]
    A-->E(Smoke Detection System)
    E-->F(Programmed with C on Arduino IDE)
    A-->G(Game Development Project)
    G-->H(Using VB.Net and SQL Server)
    A-->I(Rain Water Harvesting)
    I-->J(Theme - Land Resources)


```
---------

```mermaid  


graph TD
    A[Workshops]
    A-->K(WorldQuant University Data Science course)
    A-->M(Hardware & Networking)
    A-->N(Hobby Electronics & Robotics)


```

## Some of my favorite activites in the Summer :sunny: and Winter :snowflake: seasons

~~~ plantuml


@startmindmap
* Favorite Activities
 *[#Orange] Summer Activities
  *[#Orange] Hiking
  *[#Orange] Road Trip
  *[#Orange] Picnic in the Park
 *[#lightblue] Winter Activities
  *[#lightblue] Ice Skating
  *[#lightblue] Barbeque night with Bonfire
  *[#lightblue] Reading a book with Hot chocolate
@endmindmap


~~~
## My Todo List :pencil2:
Coming from abroad, there are a lot of initial tasks that I have to complete. Below are some of my tasks
- [x] Aachen City Registration
- [x] Open a German Bank account
- [ ] Collect my Blue card and Semester Ticket
- [ ] Apply for Residence Permit 



## Planned Itenary to explore Germany :red_car:

I have planned to explore Germany starting with the following places;
| Attractions | Location | Distance via car in kms| Entry fees |  
|:------|:------|:------|:------|
| Sea Life | Konstanz | 574,6 km | 20 Euro |
| Madame Tussauds | Berlin | 636,1 km | 26 Euro |
| German Football Museum | Dortmund | 156,5 km | 19 Euro |
| Chocolate Museum | Cologne | 78,4 km | 14 Euro |
| | | | **79 Euro** |


## This quote by Henry David Thoreau is so relatable 
> "What is the use of a house if you haven’t got a tolerable planet to put it on?"

### My motivation to pursue this course; 

![1]
1. I love technology and always think of ways to use it to solve real world problems. 
2. Construction has been one of the last industries to use digitalization and automation. 
3. I would want to build robots that could completely replace human interventions especially in the hazardous or dangerous construction sites.
4. New innovations and efficient technology could improve the face of the Construction industry on the whole. Some of the smart technology that I would like to work on are;
   * Using drones in construction to optimize and reduce human involvement in dangerous sites
   * Having a transparency and improving customer relationship by building smart contracts
   * Smart work equipments with tracking to enhance the safety of the construction workers

I found this article on the Trends in Construction industry from where some of my motivation stems from. If you are interested you can have a quick read here (https://www.bigrentz.com/blog/construction-trends)

[1]: https://learnenglishteens.britishcouncil.org/sites/teens/files/field/image/rs7376_thinkstockphotos-599686170-hig.jpg


## I am throughly enjoying being part of this Master's Program and looking forward to learning a lot :raised_hands:


## From here on are the topics that I learnt and tried out their implementations. 

## Using codeblocks with highlighting of different programming languages

~~~ json

{
  "firstName": "John",
  "lastName": "Muller",
  "age": 32
}

~~~

~~~ python

  def personal_details():
    name, age = "Sara", 29
    address = "Bangalore, Karnataka, India"
    print("Name: {}\nAge: {}\nAddress: {}".format(name, age, address))

personal_details()

~~~

~~~ javascript

let person = {
  name: 'Adam',
  age: 25,
  job: 'Programmer'
}
const { name, age } = person;
console.log(name);
console.log(age);

~~~

~~~ c

int main()  
  {
     printf("Name   : Leyla Jessica\n"); 
     printf("DOB    : June 15, 1985\n"); 
     printf("Mobile : 99-9999999999\n"); 
     return(0); 
  }

~~~

## Headers 
# First degree header
## Second degree header
To make headers in Markdown, you preface the phrase with a hash mark (#). You place the same number of hash marks as the size of the header you want. For example, for a header one, you'd use one hash mark (# Header One), while for a header three, you'd use three (### Header Three)

# Markdown Notes & Tutorial Exercises

# To make a phrase italic in Markdown, you can surround words with an underscore (_)  
Make the word "not" italic  
Writing in Markdown is _not_ that hard!  

# To make phrases bold in Markdown, you can surround words with two asterisks ( ** )  
Make the word "will" bold  
I **will** complete these lessons!  

# Links - 2 Types
1. Inline link - To create an inline link, you wrap the link text in brackets ( [ ] ), and then you wrap the link in parenthesis ( ( ) ). For example, to create a hyperlink to www.github.com, with a link text that says, Visit GitHub!, you'd write this in Markdown: [Visit GitHub!](www.github.com)

2. Reference link - As the name implies, the link is actually a reference to another place in the document. 

Example: 
Link both of the words “hurricane” to the Wikipedia entry at https://w.wiki/qYn with a reference style link:  
< Hurricane Erika was the strongest and longest-lasting tropical cyclone in the 1997 Atlantic hurricane season. >  
[Hurricane][1] Erika was the strongest and longest-lasting tropical cyclone in the 1997 Atlantic [hurricane][1] season.

[1]:https://w.wiki/qYn

# Images using Markdown

## Example of how to add an image to the git - use ![](paste the image address link)
![Digitalization in the Construction Industry](https://www.douglascompany.com/wp-content/uploads/2018/12/The-Case-for-Construction-Technology.jpg)

# Using Reference links under images

![Manual Labour in construction Industry][Manual]

![Machines in construction Industr][Machine]

[Manual]: https://www.fresh50.com/wp-content/uploads/2019/10/images2547-5d9d2baf454c8-678x381.jpg

[Machine]: https://resources.news.e.abb.com/images/2021/5/19/0/Hero_Image_-_credit_Mesh_Mould_and_in_situ_Fabricator_ETH_Zurich_2016_2017_Gramazio_Kohler_Research_ETH_Zurich.jpg

# Blockquotes  
To create a block quote, all you have to do is preface a line with the "greater than" caret (>).  
Ex:  
>"Her eyes had called him and his soul had leaped at the call. To live, to err, to fall, to triumph, to recreate life out of life!"

# Lists
## Unordered Lists - To create an unordered list, you'll want to preface each item in the list with an asterisk ( * )  
* Bricks
* Cement
* Machinery
## Ordered lists - An ordered list is prefaced with numbers, instead of asterisks.
1. Bricks
2. Cement
3. Machinery
## Nested lists 
* Calculus
  * A professor
  * Has no hair
  * Often wears green
* Castafiore
  * An opera singer
  * Has white hair
  * Is possibly mentally unwell

1. Cut the cheese  
   Make sure that the cheese is cut into little triangles.
   
2. Slice the tomatoes
   Be careful when holding the knife.  
   For more help on tomato slicing, see Thomas Jefferson's seminal essay Tom Ate Those.

# Paragraphs
1. Explicit line breaks - Two spaces will put your sentence in the next line and this is called forced line break or soft break of a paragraph
2. Consecutive lines - Add a blank line to separate 2 paragraphs

# Kramdown Notes and Exercises
kramdown has two main classes of elements: 
1. **Block-level elements** are used to create paragraphs, headers, lists and so on
2. **Span-level elements** are used to markup text phrases as emphasized, as a link and so on.

# Example of Plantuml mindmap (Code Blocks) 
Code Blocks - kramdown supports two different code block styles. One uses lines indented with either four spaces or one tab whereas the other uses lines with tilde characters as delimiters – therefore the content does not need to be indented. ~~~ use tilda's annd paste the special language that is supported by gitlab


~~~ plantuml


@startmindmap
* Debian
** Ubuntu
*** Linux Mint
*** Kubuntu
*** Lubuntu
*** KDE Neon
** LMDE
** SolydXK
** SteamOS
** Raspbian with a very long name
*** <s>Raspmbc</s> => OSMC
*** <s>Raspyfi</s> => Volumio
@endmindmap


~~~

## Definition lists  
Definition lists are started when a normal paragraph is followed by a line starting with a colon and then the definition text  

term
: definition  

Another term
:  another definition 

## Tables 
* A line starting with a pipe character (|) starts a table row.
* However, if the pipe characters is immediately followed by a dash (-), a separator line is created
* If the pipe character is followed by an equal sign (=), the tables rows below it are part of the table footer

| Items | Store | Cost |  
|:------|:------|:------|
| Shoes | Decathlon | 150 Euro |
| Jacket | Zara | 300 Euro |
| Bag | H&M | 250 Euro |
| | | Total |
| | | 700 Euro |

## HTML Elements  
(div - division or a section, used as a container for HTML elements, p - paragraph element, pre - preformatted text, to be displayed as it is)
<div style="float: right">
Master's in Construction and Robotics course description
</div>

<div>
The main objective of the programme is the development and use of automated construction machinery and robotics as the basis for innovative construction processes on construction sites
</div>
<p>
[This is a link to the course website] (https://cr.rwth-aachen.de/)
</p>
