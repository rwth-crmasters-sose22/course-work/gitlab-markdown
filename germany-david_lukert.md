# Hello world
Click on any of the :arrow_forward: to learn more about me.

<details><summary>:house:</summary>

## Personal

+ **Name:** David Lukert
+ **Age:** 26 Years 
+ **Country:** Germany
+ **Hometown:** Waldrems a village with about 2.500 inhabitants 
                Here you can see an arial view of a part of my village. 
                ![Arial view of Waldrems](/assets/httpswww.backnang.desiteBacknang-Internetgetparams_E114407604518181952backnang_header_stadtteile_waldrems.jpg)

</details>


<details><summary>:mortar_board: :construction_worker:</summary>

## Education & work experience  
### Bachelors degree  
University of Stuttgart - Architecture and Urban Planning B.Sc.   
Topic of my Bachelor thesis: Designing Responsive and Kinetic Building Envelopes 

### Work experience   
+ Working student at Ed. Züblin for the facade planning department (2,5 years)
+ Working as project engineer at Lukert GmbH, calculating and managing construction projects (2,5 years)

### Skills  
+ Rhino and Grasshopper (Advanced)
+ Autocad (Advanced)
+ Python (Beginner to intermediate)
+ Adobe Creative Cloud (Advanced)
+ C# (Beginner)
+ Microsoft Excel (Intermediate to advanced)
+ CFD Simulation (Beginner to Intermediate)

</details>


<details><summary>:soccer: :books: :guitar:</summary>

## Freetime activities 

```plantuml
@startmindmap
+[#MediumPurple] Freetime Activities
++[#Salmon] indoor
+++[#pink] loud
++++[#MistyRose] eguitar
++++[#MistyRose] drums 
++++[#MistyRose] piano
+++[#pink] quiet
++++[#MistyRose] theology books 
++++[#MistyRose] home workouts

--[#DeepSkyBlue] outdoor
---[#LightSkyBlue] summer 
----[#APPLICATION] helping at church camp 
----[#APPLICATION] sport
-----[#Azure] soccer
-----[#Azure] volleyball
-----[#Azure] spikeball
---[#LightSkyBlue] winter
----[#APPLICATION] skiing / snowboarding
----[#APPLICATION] ice-skating
@endmindmap

```

</details>


<details><summary>:muscle:</summary>

## Motivation 

```mermaid
flowchart BT
    
    A([GOAL: </br> make the construction </br> industry more efficient </br> and sustainable])
    B{interdisciplinary}
    C{tackle real problems}
    D{learn a lot of stuff}
    E(civil engineering </br> architecture </br> computer science </br> logistics </br> mechanical engineering </br> economics)
    F(autonomus machinery </br> sustainable materials </br> building in context </br> construction logistics </br> complex building systems </br> efficient project managing)
    G(python </br> machine learning </br> sql </br> computer vision </br> economic basics)


    B:::second-->A:::first  
    C:::second-->A
    D:::second-->A 
    E:::third-->B
    F:::third-->C  
    G:::third-->D

    
    classDef first fill:#FFC300, stroke: none
    classDef second fill:#FF5733, stroke: none
    classDef third fill: #C70039, stroke: none
    
``` 

</details>

<br></br>
**Looking forward to work with y'all!**  
![Hard working cat](/assets/cat-computer.gif)

```python 
#some python code here: 
name = input("Enter your name here:")
print("Hello " + name)

```










