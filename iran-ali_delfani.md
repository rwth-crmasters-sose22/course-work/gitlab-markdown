## Hey everyone :v:
It's **so nice** to have the opportunity to briefly introduce myself, and write a little about my background and interests.
### Education
- [x] Bachelo's degree in Civil Eng., Iran
- [x] M.Sc in Structural Eng., Iran
- [ ] M.Sc in Construction and robotics
### Professional experience
I've worked for more than _five years_ in construction industry; I was involved in various projects, some of which are as follows:
* Supervision of construction process
  * Residential buildings
  * Comercial buildings
* Strengthening of RC[^1] structures using FRP[^2]
* Urban tunnels & subways  

Here is an image of Tohid tunnel in Tehran (the capital city of Iran). I had the privilege to start working in this project right after graduation. 

![](https://iraninform.com/wp-content/uploads/2019/08/%D8%AA%D9%88%D9%86%D9%84-%D8%AA%D9%88%D8%AD%DB%8C%D8%AF.jpg)
#### Research interests

~~~plantuml
@startmindmap
+_ Structural Eng
++_ progressive collaps
++_ structural control
+++_ Dampers
+++_ SHM
++_ seismic design
--_ Strengthening
--_ bridge structures
--_ advanced structural analysis
@endmindmap
~~~

~~~plantuml
@startmindmap
+ construction robotics
++[#FFBBCC] additive manufacturing
+++ 3D printing
++++ concrete
++++ steel
++[#lightgreen] autonomous construction machinery
+++ collaboration of autonomous machinery
++[#lightblue] automated structures design
@endmindmap
~~~


#### Life journey


```mermaid
graph TD;
  Ali-->Germany;
  Ali-->ZeroO'clock;
  Germany-->?;
  ZeroO'clock-->?;
```
[^1]: Reinforced Concrete
[^2]: Fiber Reinforced Polymer



