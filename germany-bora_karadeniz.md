
## Writing in Markdown is not that hard!    


![markdown](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)  

  
<img src="https://mention.com/wp-content/uploads/2017/11/golf-google-alerts-mention.gif" width="450" height="250">  


### Table of Contents:  
 
 * [About me](#About me)  
   * [Basic information](#Basic information)
   * [Skills](#Skills)
   * [Hobbys](#Hobbys)
 * [Study and work](#Study and work)
    

## About me  

### Basics 

| Name | Bora Karadeniz   |
| --- | --- | 
| Nationality | Turkish  :tr: | 
| Place of birth | Dortmund/ Germany :de: | 
| Education |  Civil Engineering in Aachen |
| Current Residence | Dortmund |  




~~~ plantuml

@startmindmap
* **Skills**
** Language 
*** German : native 
*** Turkish: native
*** Englisch: **must be improved**
*** French: basics
** Software
*** AutoCAD
*** Autodesk Revit Structure
*** RIB iTwo
*** Sofistik
*** MS Office/ Excel
** Programming
*** Python
** Management methods
*** Lean Management 
@endmindmap


~~~  




 

~~~ plantuml

@startwbs
* **Hobbys**
** Sport 
***_ Soccer 
***_ Fitness/ Jogging
***_ Formula 1
***_ Tennis
** Home activities
***_  Movies/ Series
***_  Books
***_  Games (Chess, Backgammon)
** Relaxation
***_ Travel
***_ Swim/ Sauna
@endwbs

~~~ 


## Study and work  


I last worked for 2 years until 2021 in construction management at **Leonhard Weiss GmbH & Co. KG**.  

One of the projects was the _Smart Office - Airport City_ in Düsseldorf. 




















