


# About myself
## Basic facts about myself in short.

| Name | Age |Residence|Bachelor degree|
|-- |-- |--|--|
| Robin Berweiler | 27.11.1994 (27y) |Tiny room in Aachen or house in my hometown |Mechanical Engineer|

## In depth about my life
### Traveling 
I have traveled and moved quiet a lot. My two most notable trips been to Japan and to Brazil. Japan was a dream of mine which I would like to repeat some time in the future. I mean, just the food alone is reason enough to visit again. 🤤:joy:

![delicious food](https://www.tastingtable.com/img/gallery/20-japanese-dishes-you-need-to-try-at-least-once/l-intro-1644007893.jpg)

The reason why brazil seems special to me is, that I never thought that I will travel to brazil. But as my father moved to brazil, I have been traveled to brazil three times by now. Most beautiful beach in brazil I found so far is called [Pipa Beach](https://www.google.com/maps/place/Pipa+Beach/@-6.2263617,-35.0449123,357m/data=!3m1!1e3!4m5!3m4!1s0x7b28fe652749add:0x95163c192eb65218!8m2!3d-6.2268032!4d-35.045025), I really recommend to go there when you happen to make a trip to Brazil. :flag_br:

### Hobbies

I love Nature and technic alike. Therefore it's hard for me to define **the** favourate hobby that defines me. But to name the most important:

``` mermaid

graph TD;
    Hobbies-->Extroverted;
    Hobbies-->Introverted;
    Extroverted-->Diving;
    Extroverted-->Gaming;
   Introverted-->Cooking; 
   Introverted-->Gaming; 
   Introverted-->Music; 
   Introverted-->Tinkering/Learning; 



```
Following is a gif which I think describes my fascination and motivation for diving, gaming and kind of learning. This footage is from a game called [Subnatica Below Zero](https://store.steampowered.com/app/848450/Subnautica_Below_Zero/).

<img style="float: right;" src="whatever.jpg">
![Subnautica GIF](https://64.media.tumblr.com/1e3143fa7bbab328c7f8248545e4d227/56eb6e68663b4928-d3/s400x600/cafbdd1da77d0ad365a5ad23cd73271221a8c751.gifv)

Music is what keeps me going in general. I am always listening to music if I am able to. My spotify recap from last year shows this especially. :joy:  
Bottom left are the minutes of played music.

<img src="https://i.imgur.com/MB9Eb8Rh.jpg " width="300" height="500">





### Hard & Soft skills

- 🟢 CAD-Construction and Analysis (Like Fusion or Catia)
- 🟢 FEM-Analysis (Ansys)
- 🟢 English, I do everything Computer-related in English
- 🟢 Computer-Skills (I am often working with PCs and dealt with many problems)
- 🟢 Office (Excel, Word, Visio)
- 🟡 Beginner knowledge in Japanese
- 🟡 MINT-Skills

---


- 🟢 Initiative 
- 🟢 Creativity
- 🟢 Open for new Ideas / Curiosity
- 🟢 Focused worker

### Always working on, kind of like a dream

I am always trying to improving my MINT-Skills. My focus until now were on Maths, science and technics and I always neglected informatic-subjects. But I always wanted to learn some coding languages, especially Python as I know 

## Things I planned on including
- [x] Short basic introduction
- [x] Some interesting things about myself
    - [x] pictures
    - [x] Hobbies
    - [x] Skills
    - [x] Dreams
    - [x] Experiences
- [x] Self-Learning
  - [x] tables
  - [x] mindmaps
  - [x] pictures/gifs 
    - [x] resizing
    - [ ] aligning
  - [ ] [coloured text](https://bookdown.org/yihui/rmarkdown-cookbook/font-color.html) (not implemented in Markdown, but can be used when conferting to html or Latex)
  - [x] emojis
  - [x] lists
  - [ ] [interactable fillable forms](https://github.com/brikis98/wmd) (seems not to be possible on a static file) 
  - [x] Basic Code Blocks
  - [ ] Advanced Code Blocks

## Last experimentation on aligning pictures


![image alt >](/image-right.jpg)
![image alt <](/image-left.jpg)
![image alt ><](/center-image.jpg)

img[alt$=">"] {
  float: right;
}

img[alt$="<"] {
  float: left;
}

img[alt$="><"] {
  display: block;
  max-width: 100%;
  height: auto;
  margin: auto;
  float: none!important;
}

