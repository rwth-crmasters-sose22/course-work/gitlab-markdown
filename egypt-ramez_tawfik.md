# [**Road to CR Master Program :construction: **](https://cr.rwth-aachen.de/)


> its a journy :tent: that started right after graduation from bachelor seeking the right program, that would give a head start for the future, CR master  
program is _**from my point of view :eyes:**_ capable to such thing, that is why I have enrolled in.
>
>Yet so far the program is a little overwellming and in just 2 weeks I am fed by a lot of information that I need to reorganize for my personal   
benfit latter on in the course lifetime.

## Interests for CR Program

_The Program is intresting for me in so many points, as it is a merge between Computer Science and Construction in general, and I have to admit the   world is heading to digitalization so by mixing the construction field that I am in with CS that would sure place me in a different yet   expectional field._


### Kindly find Below Examples for Robotics in general
------------------------------------------------------
- [ ] [Real Life Human Robotics 1](https://vimeo.com/651929733?embedded=true&source=vimeo_logo&owner=63229520)
- [ ] [Real Life Human Robotics 2](https://vimeo.com/652071809?embedded=true&source=vimeo_logo&owner=63229520)
- [ ] [Robotics Different Use in Real life 1](https://www.bostondynamics.com/products/spot)
- [ ] [Robotics Different Use in Real life 2](https://www.youtube.com/watch?v=rEKI6qk7VoM&t=12s&ab_channel=BostonDynamics)

_Basicly those are quite an advanced level for me as a robotics example but I would still like to just have a small segement of those applications  in my projects in the near future in a way that help the civil engineering industry._


### That shall be the expected outcome

```mermaid
graph LR
    A[Ramez] -->B(Civil Engineer)
    B --> C{Robotic expectional Level}
    C -->D[Increase Level of Accuracy]
    C -->E[High QC level]
    C -->F[Fast Project Tracking]
    A --> X[CS]
    X --> C
    A  --- |The Fun Part| Y[Travilling] 
    Y --- Z[GYM]
```

# GOOD BYE

<img src="https://media1.giphy.com/media/PlLanl8Bzcvr14IfjJ/giphy.gif?cid=ecf05e47fc3j39qt4xumu7nu2b1ozspvyikig4qxuay8rol6&rid=giphy.gif&ct=g">














# [Header 1](https://www.visualpharm.com/free-icons/header%201-595b40b65ba036ed117d1d92)
# Header 1
## Title 2

### Title 3

_this is a sentence that resemble an italic.tatattataataatt._

**this is a sentence that resemble a Bold.tatattataataatt.**

_**this is a sentence that resemble an italic and Bold.tatattataa%taatt.**_

[DIET is FUN](https://www.google.com/search?q=no+it+is+not&sxsrf=APq-WBtP1YnVqcHQzjFZmWQezqolEblVQA:1649853103748&source=lnms&tbm=isch&sa=X&ved=2ahUKEwiPrLjhhZH3AhUSg_0HHU1ABFAQ_AUoAXoECAEQAw&biw=1536&bih=714&dpr=1.25)



[Mutli links same place 1][x]  
[Mutli links same place 2][y]  
[Mutli links same place 1][x]    
[Mutli links same place 2][y] 

[x]:http://www.google.com
[y]:http://www.facebook.com

**Enter in the Edit code section and change the numbers in the website!**  
![Bears are changable](https://placebear.com/200/200)

> "MY life Bio is Loading...."


This is the List section  
* unordered 1  
 *unordered 1.1  
 *unordered 1.2  
 *unordered 1.3  
* unordered 2 
 1. ordered 2.1  
 2. ordered 2.2  
 3. ordered 2.3  
* unordered 3 

HEX: `#RGB[A]`
`#F00`

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```
```mermaid
graph TD
    A[Christmas] -->|Get money| B(Go shopping)
    B --> C{Let me think}
    C -->|One| D[Laptop]
    C -->|Two| E[iPhone]
    C -->|Three| F[fa:fa-car Car]
```

```mermaid
flowchart TD
    Start --> Stop
```
```mermaid
flowchart LR
    id1[(Database)]
```

```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2014-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2014-01-12  , 12d
    another task      : 24d
```
:relaxed:

| Left Aligned | Centered | Right Aligned |
| :---         | :---:    | ---:          |
| Cell 1       | Cell 2   | Cell 3        |
| Cell 4       | Cell 5   | Cell 6        |
