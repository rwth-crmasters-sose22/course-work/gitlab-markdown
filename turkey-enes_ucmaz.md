## HELLO ALL!
### I have just followed the tutorials!
#### _And that's what I have._

[If you want to change your **mood**](https://www.youtube.com/watch?v=AiT7btxnyUQ&ab_channel=Hangar)[If you want to change your **mood**](https://www.youtube.com/watch?v=AiT7btxnyUQ&ab_channel=Hangar)

# So let's start with boring charts!

## Usual Information

| ------ | /////////// |
| ------ | ----------- |
| **Age**   | 26 |
| **From** | Alasehir, a small town of Manisa province in Turkey |
| **Studies**    | Bachelor of Architecture |

### Cross out the lines

~~You cannot express yourself with charts or stereotypical tools..~~

### Let's try something else!

- [ ] strawberry
- [ ] chicken
- [x] tuna fish
- [x] milk
- [ ] bread
- [ ] watermelon

### Nahhh!!! It wouldn't work too

| Like        | Neutral        | Dislike    |
| ------------- |:-------------:| ---------:|
| Sitcom         | Action Movie         |     Soap Opera |
| Autumn         | Spring         |   Winter    |
| Honesty         | Secrecy         |        Smugness |
| Cat         | Dog         |     Mosquito |
| Basketball         | Football         |   Badminton    |
| Space Rock        | Rap         |        Pop |
| Steak         |  Chips        |     Wasabi |

> While trying to escape from the classical narrative,
>> we have created a narrative with different boundaries which are,
>>
> * Ignoring different alternatives by using sharp expressions such as like and dislike.
> * Make a generalization with few words without details.

> __Let's try to get to know me with a different tool.__

## VISUAL and VERBAL EXPLANATION!
**Music,** especially **singing,** is one of the biggest passions in my life. Having been interested in music since primary school, I have worked as a soloist and choirist in various concerts in Turkey and the Netherlands. As a choir, I performed my last concert with the **Bosphorus Philharmonic Orchestra** on the theme of "Game of Thrones". I am and will keep my interest in music, which will always occupy an important place in my life, by improving myself and feeding it with various sources.

I wanted to add the gif below because I think it describes the **vision** that architecture gave me, my **interest** in digital art, my **passion** for music and my desire to **explore** different cultures in a single image.

![Visual Explanation](/assets/enes1.gif)

With my interest in **basketball** from an early age, I met **wrestling** towards the end of my primary school years, after which I became a licensed athlete. I continued my passion for basketball as an amateur after playing with the school team in primary school. But I wrestled in various competitions from primary school to the end of high school. My interest in these two sports continues as an **audience** since my university years. My interest in sports strengthened my **competitive** side and **self-discipline.**

![A bit strange huh?](/assets/enes2.jpg)

As strange as it may seem, this is how basketball was played during wrestling training. **No fouls,** **no timeout,** and **hours of gameplay!** :joy:

## Finally, let me summarize myself with an amateur diagram! :sweat_smile: [**CYL!**](https://www.youtube.com/watch?v=nA0fXQDKyho&ab_channel=DIEMONDOTCOM) 

```mermaid
graph TD
    A[Enes] -->|Introduce yourself| B(Let's Try)
    B --> C{Favourites}
    C -->|Music| D[Progressive rock]
    C -->|Sport| E[Basketball]
    C -->|Food| F[Kidney Bean]







