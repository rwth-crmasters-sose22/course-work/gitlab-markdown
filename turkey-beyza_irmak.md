# About Me!
Hello to everyone! My name is *Beyza Irmak* and I am 26 years old.

I was born and raised in **Denizli, Turkey.**

I studied Architecture at **Istanbul Technical University.** 

You can find more academic information about me on my [LinkedIn page.](https://www.linkedin.com/in/beyzairmak/)

![Funny House Construction](https://media3.giphy.com/media/xZsLh7B3KMMyUptD9D/200.gif)


In my spare time, I love watching TV series and movies! I love to categorize and rate what I watch. You can also rate the movies you are interested in on Letterboxd site and share your own lists. [Here is a link to my Letterboxd page!](https://letterboxd.com/beyzairmak/)

I also mostly like to watch sitcoms. Friends is one of my favorite sitcoms, as are many people.
Below you can find my favorite episodes of Friends, which is my favorite TV series and that I have watched many times.



### My Favorite Episodes
* The One Where Everybody Finds Out
* The One at the Beach
* The One Where Ross Is Fine
* The One with Unagi
* The One Where No One's Ready
* The One With the Cop
* The One With the Football

[If you want to be energized with the intro music of the series.](https://www.youtube.com/watch?v=Xs-HbHCcK58&ab_channel=edisonlsm)


![Friends](https://www.webtekno.com/images/editor/default/0002/98/662572abd54f1fff76cea15303690df8e6e369c1.jpeg)









# Tutorial

## Subtitle

### Subtitle 

Writing in Markdown is not that hard!

__Writing in Markdown is not that hard!__

_Writing in Markdown is not that hard!_

**Writing in Markdown is not that hard!**

*Writing in Markdown is not that hard!*

**_My name is Beyza Irmak. I studied architecture as a bachelor's degree._**

The name of the school I studied in Istanbul is _Istanbul Technical University._

[Visit GitHub!](www.github.com)

[Google](www.google.com)

[You're **really, really** going to want to see this.](www.dailykitten.com)

#### The Latest News from [the BBC](www.bbc.com/news)

 Here's [a link to something else][another place].
     Here's [yet another link][another-link].
     And now back to [the first link][another place].

     [another place]: www.github.com
     [another-link]: www.google.com




