# Abid Jan
## Personal Information
* From Afghanistan :af:
* 27 Years old
* INTJ Personality type

## Professional Information

### Education
|      Field of study   |      Degree      |  University|    Duration | 
|------------------|----------------|---------|---------|
| Mechanical Engineering _Design & Development_ |  M.Sc. | Hochschule Rhein-Waal | 2019-2021 |
| Mechanical Engineering |    B.Sc.   |  Mersin University | 2013-2017 |

### Work Experience
* Experienced in:  
  - [x] Machine Design
  - [x] R & D
  - [x] Product Development
  - [x] Laser-cutting Programming
  - [ ] Coding


### Languages
| Language | Level |
| ---      | ---      |
| Pashtu   | Native   |
| English   | Fluent   |
| German   | B2.1   |
| Turkish   | Fluent   |
| Dari   | Fluent   |
| Arabic   | Beginner   |


For more information regarding my educational and professional background feel free to visit my [LinkedIn](https://www.linkedin.com/in/abid-habibjan/) profile :wink:.


## Hobbies

@startmindmap
* Hobbies
** Painting
*** With acrylic colors
*** I am not Da Vinci :joy:
** Swimming
** Jogging
** Cooking
** Reading about psychology
@endmindmap
