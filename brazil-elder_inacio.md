---
title: About Front Matter
example:
  language: yaml
---

_hello from brazil_

**hello from germany**

_Of course,_ she whispered. Then, she shouted: **All I need is a little moxie!**

**_This is unbelievable_**

# This is header one
## This is header one
### This is header one
#### This is header one
##### This is header one
###### This is header one

#### Colombian Symbolism in One Hundred Years of Solitude
#### Here's some words about the book _One Hundred Years_

[Search for it].(www.google.com)

# **This is a little bit about me** :bear:
My name is **Elder** and I come from _Brazil_ and my mother language is _Portuguese_. However, I speak also _English_, _Spanish_, and I am learning **_German_**, which is my goal right now.

![Elder](https://scontent-dus1-1.xx.fbcdn.net/v/t31.18172-8/22555661_1695489200481443_9166172412747492660_o.jpg?_nc_cat=105&ccb=1-5&_nc_sid=09cbfe&_nc_ohc=sZjZXvGH_24AX86Af18&tn=qVXGqdpYzK1GfoGi&_nc_ht=scontent-dus1-1.xx&oh=00_AT8QzFrWTXaePPwDfmBVKVYmMvEPoFc6K7S9SHW_-fT51g&oe=62817927)

My **[Linkedin](https://www.linkedin.com/in/elder-inacio-446398126/)** has a little bit more about my work experience.

I like Art very much and my favorite painters are Van Gogh and Pablo Picasso

![Van Gogh](https://amsterdamyeah.com/wp-content/uploads/2020/01/visiting-the-Van-Gogh-museum-in-Amsterdam.jpg)

![Picasso](/uploads/d7a9d29a14cbcbe10d423301e47062d6/image.png)

Although Van Gogh himself said 
>"I feel there is nothing more truly artistic than loving people" 

I also like to practice all kind of sports and my favorite one is soccer. One of my favorite players is Ronaldo Phenomenon


Other sports that I like pretty much are:

* Hiking

* Volleyball

* Running

* Swimming

Throughout my life I have lived in different cities as it shows the following diagram:

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow --> Aachen
  Aachen -- Choice1 --> Germany
  Aachen -- Choice2 --> EUA
  
  end

  subgraph "Main Graph"
  Node1[Carmo MG - Brazil] --> Node2[Campinas SP - Brazil]
  Node2 --> SubGraph1[Belo Horizonte MG - Brazil] 
  SubGraph1Flow[Stuttgart Germany]
  SubGraph1 --> FinalThing[Carmo MG - Brazil]
   
end
```
I started my master studies and I have taken courses like:

- {+ Computer Vision 1 +}
- [+ Additive Manufactoring +]
- {- Design Driven Project -}
- [- Digital and Soft Skills -]

Testing PlantUML

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```
Testing task of list

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2

Testing Table of Contents
This sentence introduces my wiki page.

[[_TOC_]]

## My first heading

First section content.

## My second heading

Second section content.
