 # _**The journey of my life**_

I'm _**Somayyeh**_. If you have trouble pronouncing it, you can call me _**Somi**_ :woman_tone1: .   
I was born in _**Iran**_ and lived there. I studied _**Mechanical Engineering**_ . I researched on "_nonlinear vibration_" and its effect on systems. Also, I have seven years of work experience as a Mechanical engineer and  now I'm here to develop my _**skills and knowledge**_.  
  
# I would like to talk about these topics below:  

* Education   

* Work experience  

* Teaching experience  

* Hobbies  

## Education :mortar_board:
|      Field of study   |      Degree      |  Duration|
|------------------|:----------------:|---------:|
| Mechanical Engineering-Applied Design |  Master | 2011-2014 |
| Mechanical Engineering-Solid Design |    Bachelor   |   2007-2011 |  

## Work Experience :hammer_and_wrench:    

```mermaid  
graph TD
   C(Work Experience)
  C -->|five years| D[Mechanical Expert]--> B(Iran Railway Company)
  C -->|one year| E[Quality Control Expert]--> A(Industrial Filters Company)
  C -->|on year| F[Quality Control Manager]--> A(Industrial Filters Company)

```

  
## Teaching Experience :pencil: 
```mermaid

flowchart TD
    A[/Strength of Material\]
B[\Thermodynamics/] 
 C[/Applied Statistics\]
```
  
## Hobbies

When I was a child, my biggest hobby was **playing chess**. 

<img src="https://media.istockphoto.com/vectors/cartoon-character-playing-chess-game-vector-id1194888836?k=20&m=1194888836&s=612x612&w=0&h=GpNGGkDey9FnYbjj7BxWmdkCko7s0V7dwAACbZ9YehA=" width="300" height="200" />)

Participating in chess competitions was one of my interests.  



But **my new hobby is cycling**. I have some friends and We want to plan cycling trips to different European cities.
  
<img src="https://i.guim.co.uk/img/media/d9d110c7fdbd9eee4bdd40be444d916c8b6b4810/0_257_5100_3059/master/5100.jpg?width=1020&quality=85&auto=format&fit=max&s=fd475ff225c59d9f50c75e32fdc20234" width="300" height="200" />)   
 
It would be **amazing**.  

[Here you can find more information about cycling tours!](https://www.biketours.com/)




