# Table of Contents
1. [In Class Examples](#In Class Examples)
2. [Markdown Basic Tutorial](#Markdown Basic Tutorial)  
   - [Bold and italic](#Bold and italic)
   - [Headers](#Headers)
   - [Links](#Links)
   - [Images](#Images)
   - [Quotes](#Quotes)
   - [List](#List)
3. [Markdown Extended Syntax](#Markdown Extended Syntax)
   - [Tables](#Tables)
   - [Fenced Code Blocks](#Fenced Code Blocks)
   - [Footnotes](#Footnotes)
   - [Definition list (it din't work)](#Definition list)
   - [Strikethrough](#Strikethrough)
   - [Task list](#Task list)
   - [Emojis](#Emojis)
   - [Hightlights (it din't work)](#Hightlights)
   - [Subscript and Superscript (it din't work)](#SubSuper)
4. [Mermaid](#Mermaid)  
   - [Flowchart](#Flowchart)



## *In Class Examples* <a name="In Class Examples"></a>

# Big Title

Big Title
=========

## title

title
-----

## *Markdown Basic Tutorial* <a name="Markdown Basic Tutorial"></a>

#### Bold and italic <a name="Bold and italic"></a>


*this* is **what**  
i _want_ to __say__

Writing in Markdown is *not* that hard!

I **will** complete these lessons!

"*Of course*," she whispered. Then, she shouted: "All I need is **a little moxie**!"

If you're thinking to yourself, **_This is unbelievable_**, you'd probably be right.

#### Headers<a name="Headers"></a>

# Header one
## Header two
### Header three
#### Header four
##### Header five
###### Header six

#### Colombian Symbolism in *One Hundred Years of Solitude*

Here's some words about the book _One Hundred Years..._.

#### Links <a name="Links"></a>


[Search for it.](www.google.com)

[You're **really, really** going to want to see this.](www.dailykitten.com)

#### The Latest News from the [BBC](www.bbc.com/news)

Do you want to [see something fun][a fun place]?

Well, do I have [the website for you][another fun place]!

[a fun place]: www.zombo.com
[another fun place]: www.stumbleupon.com

#### Images <a name="Images"></a> 

![A pretty tiger](https://upload.wikimedia.org/wikipedia/commons/5/56/Tiger.50.jpg)

![Black cat][Black]

![Orange cat][Orange]

[Black]: https://upload.wikimedia.org/wikipedia/commons/a/a3/81_INF_DIV_SSI.jpg
[Orange]:http://icons.iconarchive.com/icons/google/noto-emoji-animals-nature/256/22221-cat-icon.png

#### Quotes <a name="Quotes"></a> 

I read this interesting quote the other day:

>"Her eyes had called him and his soul had leaped at the call. To live, to err, to fall, to triumph, to recreate life out of life!"

>Once upon a time and a very good time it was there was a moocow coming down along the road and this moocow that was coming down along the road met a nicens little boy named baby tuckoo...
>
>
>He was baby tuckoo. The moocow came down the road where Betty Byrne lived: she sold lemon platt.

>He left her quickly, fearing that her intimacy might turn to jibing and wishing to be out of the way before she offered her ware to another, a tourist from England or a student of Trinity. Grafton Street, along which he walked, prolonged that moment of discouraged poverty. In the roadway at the head of the street a slab was set to the memory of Wolfe Tone and he remembered having been present with his father at its laying. He remembered with bitterness that scene of tawdry tribute. There were four French delegates in a brake and one, a plump smiling young man, held, wedged on a stick, a card on which were printed the words: *VIVE L'IRLANDE*!

#### List <a name="List"></a> 

* Flour
* Cheese 
* Tomatoes

1. Cut the cheese
2. Slice the tomatoes
3. Rub the tomatoes in flour

1. Cut the cheese
1. Slice the tomatoes
1. Rub the tomatoes in flour

* Azalea (*Ericaceae Rhododendron*)
* Chrysanthemum (*Anthemideae Chrysanthemum*)
* Dahlia (*Coreopsideae Dahlia*)

* Calculus
 * A professor
 * Has no hair
 * Often wears green
* Castafiore
 * An opera singer
 * Has white hair
 * Is possibly mentally unwell


1. Cut the cheese
   Make sure that the cheese is cut into little triangles.

2. Slice the tomatoes

   Be careful when holding the knife.
   For more help on tomato slicing, see Thomas Jefferson's seminal essay _Tom Ate Those_.


We pictured the meek mild creatures where  
They dwelt in their strawy pen,  
Nor did it occur to one of us there  
To doubt they were kneeling then.  

1. Crack three eggs over a bowl.  
   Now, you're going to want to crack the eggs in such a way that you don't make a mess.  
   If you _do_ make a mess, use a towel to clean it up!

2. Pour a gallon of milk into the bowl.  
   Basically, take the same guidance as above: don't be messy, but if you are, clean it up!


### *[Markdown Extended Syntax](https://www.markdownguide.org/extended-syntax/)* <a name="Markdown Extended Syntax"></a>

#### Tables <a name="Tables"></a>

| A | B | C |
|---|---|---|
|1|2|3|

[Table generator](https://www.tablesgenerator.com/markdown_tables)

| A | B | C |
|---|---|---|
| 1 | 2 | 3 |
| 4 | 5 | 6 |

Alignment

| Left | Center | Right |
|:---|:---:|---:|
| 1 | 2 | 3 |
| 4 | 5 | 6 |


#### Fenced Code Blocks <a name="Fenced Code Blocks"></a>

Code block

      def square (x):
         return x**2
        
      x1 = int(input ("x1 = "))
      x2 = square (x1)
      print ("the square of "+ str(x1) + " is "+ str(x2))


Fenced Code block

```
def square (x):
   return x**2
        
x1 = int(input ("x1 = "))
x2 = square (x1)
print ("the square of "+ str(x1) + " is "+ str(x2))

```

Syntax Highlight


```python
def square (x):
   return x**2
        
x1 = int(input ("x1 = "))
x2 = square (x1)
print ("the square of "+ str(x1) + " is "+ str(x2))

```

#### Footnotes <a name="Footnotes"></a>

Text1 [^1], Text2 [^2] , Text3 [^3]

[^1]: footnote1
[^2]: footnote2
[^3]: footnote3

#### Definition list <a name="Definition list"></a>

Term1
: definition1

Term2 
: definition2.1
: definition2.2

do not work?

Guide example

First Term
: This is the definition of the first term.

Second Term
: This is one definition of the second term.
: This is another definition of the second term.

#### Strikethrough <a name="Strikethrough"></a>

To ~~Cross~~ text use 2 ~ in each side

#### Task list <a name="Task list"></a> 

- [x] Task 1
- [x] Task 2 
- [ ] Task 3 
- [ ] Task 4

#### Emojis <a name="Emojis"></a>

2 ways to add emojis :
- Copy-Paste emoji. [Emojipedia](https://emojipedia.org/)
- Use emoji Shortcut.


Emoji Copy-Paste

Grinning face 😀  
Red heart ❤️  


Emoji Shortcut

Use :Shortcut:.  
Shortcut  can be found in this [link](https://gist.github.com/rxaviers/7360908)

Grinning face :grinning:   
Red heart :heart:  

#### Highlights <a name="Highlights"></a>

To ==highlight== text use 2 = on each side of the text to ==hightlight==.

it doest seems to work.

guide example 

I need to highlight these ==very important words==.

#### subscript and Superscript <a name="SubSuper"></a>

for subscript use 1 ~ on each side, for superscript use 1 ^ on each side.

x~1~ = x~2~^2^

it doest seems to work.

Guide example 

H~2~O  
X^2^

using 1 ~ crosses the text as using 2.


## *Mermaid* <a name="Mermaid"></a> 


#### Flowchart <a name="Flowchart"></a> 



```mermaid
flowchart LR
  A-->B
  A-->C
  C-->D
  D-->A
  B-->D
  D-->E
  D-->F
```

##### Shape of nodes

```mermaid
flowchart BT

  id1(A)-->id2[(B)]
  id1(A)-->id3([C])
  id3([C])-->id4{D}
  id4{D}-->id1(A)
  id2[(B)]-->id4{D}
  id4{D}-->id5[\E/]
  id4{D}-->id6[/F/]
```

##### Links between Tasks

```mermaid
flowchart TB
  A --- B
  A -.-> C
  C -- Text --> D
  D ==> A
  B o--o D
```

##### Linking

```mermaid
flowchart LR
  A-->B & C --> D --> A & E & F
```


