 # Hello, I am Su Htoo Inzali.  :fallen_leaf: 


  > I was born in **Burma** and grew up in **Singapore**.
  >
  > I can speak Burmese, English. and a little bit of Japanese. 
  >
  > I previously studied Diploma in _Architectural Technology_ and graduated with bachelor degree in _Construction Management_. 
  >
  > I worked as a BIM specialist in Singapore for 6 years. 

After stucking in one place for two years, being unable to travel like most people, I needed a break from 9 to 5 life and couldn't wait to be in new environment and learn new things. This program, which I can mix a little bit of every other displines I've ever heard, is just perfect for me, regradless of that I can keep up or not. :laughing: :laughing: :laughing:


   Anyway, these are my hobbies I can think of for now haha :point_down:

<img src= "https://www.savingtour.online/wp-content/uploads/2020/05/Travel.jpg" width="200" height="200">
<img src= "https://unbumf.com/wp-content/uploads/2018/10/Watch-Movies_UnBumf.jpg" width="200" height="200">
<img src= "https://cdn.haproxy.com/wp-content/uploads/2019/05/Stat-Explorer.png" width="200" height="200">

_travelling | movies | exploring new things_


>And, the Fields I am interested in :point_down:
>
```mermaid

flowchart LR
    id1(INNOVATIVE TECHNOLOGIES)---id2(SUSTAINABLE DEVELOPMENT)---id3(BUILDING & CONSTRUCTION)
    id4((BIM))-.->id1
    id5((COMPUTATION))-.->id1
    id6((ROBOTICS))-.->id1
    id7((AUTOMATION))-.->id1
    id3<-.-id8((RAMMED EARTH))
    id3<-.-id9((STRABALE))
    id3<-.-id10((BAMBOO))
    id3<-.-id11((TIMBER FRAME))

```
