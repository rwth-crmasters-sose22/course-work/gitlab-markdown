# Hello! :smile:

My name is *Noel* I'm **23** years old, I did my bacherlor's degree in **Architecture**.             
I love learning new things, having new experiences and meeting new people.

<img src="https://i.ibb.co/QQngmGn/Picture-of-me.jpg" alt="Picture-of-me" height="400" width = "400" />

## My Interests :star:

![](http://www.plantuml.com/plantuml/svg/9Or12i9034NtESMiP-CLgeZYGl0AOHfg5fEHv1VqzZRjxYNynvi2xEXgS-SFKItFNtaOn2KGL6kvZA_QadILqCMeRlXYDUGxZxEtjq8QLfVJxkhmO-PZNXjAsZSrjYQ9T-xfqYHRzIZ_0G00)

## Some Rap from my city for your Leisure  :musical_note:
<img src="https://i1.sndcdn.com/avatars-000731214079-zs9ami-t500x500.jpg" alt="1" height="400" width = "400" />
[Link to song](https://www.youtube.com/watch?v=7wtBtXTGJQk)  

<img src="https://pbs.twimg.com/media/EUlZ4e7UUAA76nQ.jpg" alt="1" height="400" width = "400" />
[Link to song](https://www.youtube.com/watch?v=53o0g_nniFQ)    
  
  
  
Thank you! Looking forward to learn more Markdown :smile:
  


